#!/bin/bash

echo 'build starting'

echo 'moving to serviceone and running npm run build'

cd ./src/public/serviceone
npm install
npm run build

echo 'moving node_modules to parent'
mv ./node_modules ../../../build/