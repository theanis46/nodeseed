import { controller, httpGet } from 'inversify-express-utils';

@controller('/')
export class HomeController {
  constructor(){
    console.log("HOme controller");
  }
  @httpGet('/')
  public get(): string {
    return 'Home sweet home';
  }
}
