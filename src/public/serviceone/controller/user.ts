import {
  controller, httpGet, httpPost, httpPut, httpDelete
} from 'inversify-express-utils';
import { inject } from 'inversify';

import * as express from 'express';

import { IUser } from '../service/IUser';
import types from '../service/type';
import IUserService from '../service/IUserService';

@controller('/user')
export class UserController {

  constructor(@inject(types.IUserService) 
  private userService: IUserService,
  

  // @inject(types.TraceIdValue) 
  // private readonly traceID: string
  ) { }

  @httpGet('/', types.TracingMiddleware)
  public getUsers(request: express.Request): IUser[] {
    // console.log(this.traceID)
    console.log(request.headers);
    return this.userService.getUsers();
  }

  @httpGet('/:id')
  public getUser(request: express.Request): IUser {
    return this.userService.getUser(request.params.id);
  }

  @httpPost('/')
  public newUser(request: express.Request): IUser {
    return this.userService.newUser(request.body);
  }

  @httpPut('/:id')
  public updateUser(request: express.Request): IUser {
    return this.userService.updateUser(request.params.id, express.request.body);
  }

  @httpDelete('/:id')
  public deleteUser(request: express.Request): string {
    return this.userService.deleteUser(request.params.id);
  }
}
