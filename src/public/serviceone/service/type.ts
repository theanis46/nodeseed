const types = {
    IUserService: Symbol.for('IUserService'),
    Request: Symbol.for('Request'),
    TraceIdValue: Symbol.for("TraceIdValue"),
    TracingMiddleware: Symbol.for("TracingMiddleware"),
};
export default types;
