import { injectable } from "inversify";
import { BaseMiddleware } from "inversify-express-utils";
import * as express from 'express';
 import types from "./type";


@injectable()
export class TracingMiddleware extends BaseMiddleware {
 
    public handler(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        this.bind<string>(types.TraceIdValue)
            .toConstantValue(`${ req.header('X-Trace-Id') }`);
        next();
    }
}
 