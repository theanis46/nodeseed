import { Container } from 'inversify';
import types from './type';
import IUserService from './IUserService';
import { UserService } from './UserService';
import * as express from 'express';
import { TracingMiddleware } from './TracingMiddleware';




export function configureCommonServices(container: Container, basePath: string, config?: object): Container {

    container.bind<IUserService>(types.IUserService).to(UserService).inTransientScope();
    container.bind<any>(types.Request).toConstantValue(express.request);
    container.bind<string>(types.TraceIdValue).toConstantValue('TraceId');
    container.bind<TracingMiddleware>(types.TracingMiddleware)
         .to(TracingMiddleware);
    
    return container;
}
