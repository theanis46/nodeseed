import 'reflect-metadata';
import { InversifyExpressServer, getRawMetadata } from 'inversify-express-utils';
import { Container } from 'inversify';
import * as bodyParser from 'body-parser';
import * as prettyjson from "prettyjson";


import { configureCommonServices } from './service/ioc';
//import { TracingMiddleware } from './service/TracingMiddleware';
//import * as express from 'express';


let container = new Container();
configureCommonServices(container, __dirname);

import './controller/home';
import './controller/user';

// start the server
let server = new InversifyExpressServer(container);


server.setConfig((app) => {
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
});



let serverInstance = server.build();

const routeInfo = getRawMetadata(container);

console.log(prettyjson.render({ routes: routeInfo }));
serverInstance.listen(3000);

console.log('Server started on port 3000 :)');
